## Status
[![pipeline status](https://gitlab.com/rayddteam/authelia/badges/master/pipeline.svg)](https://gitlab.com/rayddteam/authelia/commits/master)

## hello-world
Bastille template to bootstrap Authelia - The Single Sign-On Multi-Factor portal for web apps

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/rayddteam/authelia
```

## Usage
```shell
bastille template TARGET rayddteam/authelia
```
